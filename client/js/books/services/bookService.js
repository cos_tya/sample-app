'use strict';

angular
    .module('books')
    .factory('bookService', ['$http', function ($http) {

        function errorHandler(errorObj) {
            //just logging for now, can be moved to a separate module
            console.log('Something went wrong', response.data);
        }

        function BookService() {
            this._books = [];
        }

        BookService.prototype.find = function (title) {
            var that = this;
            return $http({
                url: 'api/books',
                method: 'GET',
                params: {title: title}
            })
                .then(
                    function (response) {
                        var _payload = response.data.payload;
                        if (!_.isEmpty(_payload) && _.isArray(_payload)) {
                            that._books.splice(0, that._books.length);
                            _.each(_payload, function (book) {
                                that._books.push(book);
                            });
                        } else {
                            errorHandler(response.data);
                        }
                    },
                    errorHandler
                )
        };

        BookService.prototype.all = function () {
            if (!this._books.length) {
                this.find('');
            }
            return this._books;
        };

        return new BookService();
    }]);
