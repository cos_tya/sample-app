'use strict';


angular
    .module('books')
    .directive('bookList', ['bookService', function (bookService) {

        function Controller() {
            this.books = bookService.all();
        }

        return {
            restrict: 'A',
            scope: {},
            templateUrl: 'js/books/views/list.html',
            controller: Controller,
            controllerAs: '$ctrl'
        }
    }]);
