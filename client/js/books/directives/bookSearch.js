'use strict';


angular
    .module('books')
    .directive('bookSearch', [function () {

        function Controller($scope, bookService) {
            this._scope = $scope;
            this._bookService = bookService;

            this.schema = {
                type: 'object',
                properties: {
                    title: {
                        type: 'string',
                        minLength: 2,
                        maxLength: 50,
                        title: 'Find by title:'
                    }
                }
            };

            this.formOptions = {
                formDefaults: {
                    disableSuccessState: true,
                    feedback: false
                }
            };

            this.form = ['*'];

            this.model = {};

        }

        Controller.prototype.onSubmit = function (form) {
            this._scope.$broadcast('schemaFormValidate');
            if (form.$valid) {
                this._bookService.find(this.model.title);
            }
        };

        Controller.prototype.showAll = function () {
            this.model.title = '';
            this._bookService.find('');
        };

        return {
            restrict: 'A',
            scope: {},
            templateUrl: 'js/books/views/search-form.html',
            controller: ['$scope', 'bookService', Controller],
            controllerAs: '$ctrl'
        };
    }]);
