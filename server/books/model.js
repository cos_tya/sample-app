'use strict';

function Book(title, author, price) {
    this.title = title;
    this.author = author;
    this.price = price;
}

module.exports.default = Book;
