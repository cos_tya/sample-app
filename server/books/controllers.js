'use strict';

var bookService = require('./services').default;
var responses = require('../responses');
var logger = require('../logger');
var validator = require('validator');

module.exports.searchBooks = function (req, res) {
    var query = req.query;
    var title = '';

    /*validation*/
    if (query.hasOwnProperty('title')) {
        var _title = validator.trim(query.title);
        if (validator.isLength(_title, {min: 0, max: 50})) {
            title = validator.escape(_title);
        }
    }

    bookService.findByTitle(title)
        .then(function(books) {
            res.json(new responses.SuccessResponse(books));
        })
        .fail(function (error) {
            logger.error('Errors:', error);
            res.json(new responses.ErrorResponse());
        })
        .done();
};
