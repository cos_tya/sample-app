'use strict';

var express = require('express');
var router = express.Router();

router.get('/', require('./controllers').searchBooks);

module.exports = router;
