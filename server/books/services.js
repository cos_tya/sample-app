'use strict';

var Q = require('q');
var _ = require('lodash');
var Book = require('./model').default;

function BookService() {
    this._books = [
        new Book(
            'Astrophysics for People in a Hurry',
            'Neil deGrasse Tyson',
            8.91
        ),
        new Book(
            'Undeniable: Evolution and the Science of Creation',
            'Bill Nye',
            12.99
        ),
        new Book(
            'Giant of the Senate',
            'Al Franken ',
            8.07
        ),
        new Book(
            'The Habit Blueprint: 15 Simple Steps to Transform Your Life',
            'Patrik Edblad',
            2.99
        ),
        new Book(
            'Spirituality: A Working Scientific Hypothesis For Our Lives',
            'Saravanan Rajaboja',
            2.99
        ),
        new Book(
            'Death by Black Hole: And Other Cosmic Quandaries',
            'Neil deGrasse Tyson',
            8.61
        )
    ];
}

/**
 * finds books by title,
 * if no book found returns all books
 * @param title
 * @return {Promise.<Array>} found array of books
 */
BookService.prototype.findByTitle = function (title) {
    var foundBook = _.filter(this._books, function (book) {
        return new RegExp(title, 'i').test(book.title);
    });
    if (foundBook.length === 0) {
        foundBook = this._books;
    }
    return Q.resolve(foundBook);
};

module.exports.default = new BookService();
