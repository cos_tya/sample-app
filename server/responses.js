'use strict';

var _ = require('lodash');


function AbstractResponse() {
    this.errors = [];
    this.payload = {};
}


function SuccessResponse(payload) {
    AbstractResponse.call(this);
    this.payload = payload;
}

SuccessResponse.prototype = Object.create(AbstractResponse.prototype);
SuccessResponse.prototype.constructor = SuccessResponse;

module.exports.SuccessResponse = SuccessResponse;


/**
 *
 * @param errors can be array or just sinlge error
 * @constructor
 */
function ErrorResponse(errors) {
    AbstractResponse.call(this);
    if (_.isUndefined(errors)) {
        errors = 'Something went wrong';
    }
    if (!_.isArray(errors)) {
        errors = [errors];
    }
    this.errors = errors;
}

ErrorResponse.prototype = Object.create(AbstractResponse.prototype);
ErrorResponse.prototype.constructor = ErrorResponse;

module.exports.ErrorResponse = ErrorResponse;
