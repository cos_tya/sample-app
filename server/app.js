var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '..', 'client')));
app.use('/vendor', express.static(path.join(__dirname, '..', 'bower_components')));

// load routes
require('./routes')(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    var _logger = require('./logger');
    _logger.error('Unexpected Error', err);
    var ErrorResponse = require('./responses').ErrorResponse;
    var error = new ErrorResponse();
    res.json(error);
});

module.exports = app;
