# TEST Application

This is a basic web application written with ExpressJS.
Landing page 'index.html' consists of Angular(v1) application.
Server API is mapped onto '/api' path.

Application has very basic functionality of displaying list of books inventory.


## to run server
    $ npm install
    $ bower install
    $ DEBUG=test-task:* npm start
    
web server will be available under: `http://localhost:3000`
    
## to run tests

### server tests
    $ npm install
    $ npm run test-server