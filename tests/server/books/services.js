'use strict';

var sinon = require('sinon');
var assert = require('assert');
var rewire = require('rewire');
var BookService = rewire('../../../server/books/services').__get__('BookService');
var Book = require('../../../server/books/model').default;


function TestBookService() {
    BookService.call(this);
    this._books = [
        new Book(
            'test book 1'
        ),
        new Book(
            'test book 2'
        ),
        new Book(
            'some new book'
        )
    ];
}

TestBookService.prototype = Object.create(BookService.prototype);
TestBookService.prototype.constructor = TestBookService;


describe('Find books', function () {

    var bookService;

    beforeEach(function() {
        bookService = new TestBookService();
    });
    
    it('Should return 1 book', function (done) {
        bookService.findByTitle('Some')
            .fail(function (err) {
                console.error(err);
                assert(!err, 'We do not expect error here');
            })
            .then(function (foundBooks) {
                assert.equal(foundBooks.length, 1);
            })
            .done(done);
    });

    it('Should return 2 books', function (done) {
        bookService.findByTitle('tEst')
            .fail(function (err) {
                console.error(err);
                assert(!err, 'We do not expect error here');
            })
            .then(function (foundBooks) {
                assert.equal(foundBooks.length, 2);
            })
            .done(done);
    });

    it('Should return all books if can not find', function (done) {
        bookService.findByTitle('unreal')
            .fail(function (err) {
                console.error(err);
                assert(!err, 'We do not expect error here');
            })
            .then(function(foundBooks) {
                assert.equal(foundBooks.length, 3);
            })
            .done(done);
    });
});